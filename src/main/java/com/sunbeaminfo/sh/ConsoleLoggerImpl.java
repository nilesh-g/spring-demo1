package com.sunbeaminfo.sh;


public class ConsoleLoggerImpl implements Logger {
	@Override
	public void log(String message) {
		System.out.println("Console: " + message);
	}
}
