package com.sunbeaminfo.sh;

import org.springframework.beans.factory.xml.XmlBeanFactory;
import org.springframework.context.support.ClassPathXmlApplicationContext;
import org.springframework.core.io.ClassPathResource;

public class Sp01Main {
	public static void box_main(String[] args) {
		BoxImpl b1 = new BoxImpl();
		b1.setLength(5);
		b1.setBreadth(4);
		b1.setHeight(3);
		int res1 = b1.calcVolume();
		System.out.println("Vol 1 : " + res1);

		BoxImpl b2 = new BoxImpl(8, 6, 4);
		int res2 = b2.calcVolume();
		System.out.println("Vol 2 : " + res2);	
		
		/*
		ClassPathResource res = new ClassPathResource("/box-beans.xml");
		XmlBeanFactory factory = new XmlBeanFactory(res);
		
		BoxImpl b3 = (BoxImpl) factory.getBean("b3");
		int res3 = b3.calcVolume();
		System.out.println("Vol 3 : " + res3);

		BoxImpl b4 = (BoxImpl) factory.getBean("b4");
		int res4 = b4.calcVolume();
		System.out.println("Vol 4 : " + res4);
		
		BoxImpl b5 = (BoxImpl) factory.getBean("b3");
		*/
		
		/*
		ClassPathXmlApplicationContext ctx;
		ctx = new ClassPathXmlApplicationContext("/box-beans.xml");
		
		BoxImpl b3 = (BoxImpl) ctx.getBean("b3");
		int res3 = b3.calcVolume();
		System.out.println("Vol 3 : " + res3);

		BoxImpl b4 = (BoxImpl) ctx.getBean("b4");
		int res4 = b4.calcVolume();
		System.out.println("Vol 4 : " + res4);

		ctx.close();
		*/
	}
	
	public static void account_main(String[] args) {
		ClassPathXmlApplicationContext ctx = new ClassPathXmlApplicationContext("/account-beans.xml");
		AccountImpl a1 = (AccountImpl) ctx.getBean("a1");
		System.out.println(a1);
				
		AccountImpl a2 = (AccountImpl) ctx.getBean("a2");
		System.out.println(a2);
		
		AccountImpl a3 = (AccountImpl) ctx.getBean("a3");
		System.out.println(a3);
		
		a3.deposit(2000);
		System.out.println(a3);

		ctx.close();
	}

	public static void main(String[] args) {
		ClassPathXmlApplicationContext ctx;
		ctx = new ClassPathXmlApplicationContext("/autowire-beans.xml");
		AccountImpl a1 = (AccountImpl) ctx.getBean("a1");
		
		System.out.println(a1);
		a1.withdraw(3000);
		System.out.println(a1);
		
		ctx.close();
	}
}
