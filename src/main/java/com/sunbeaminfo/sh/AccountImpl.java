package com.sunbeaminfo.sh;

public class AccountImpl {
	private int accId;
	private String type;
	private double balance;
	private PersonImpl accHolder;
	private Logger logger = null;

	public AccountImpl() {
		
	}

	public AccountImpl(Logger logger) {
		this.logger = logger;
	}

	public AccountImpl(int accId, String type, double balance, PersonImpl accHolder) {
		this.accId = accId;
		this.type = type;
		this.balance = balance;
		this.accHolder = accHolder;
	}

	public int getAccId() {
		return accId;
	}

	public void setAccId(int accId) {
		this.accId = accId;
	}

	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}

	public double getBalance() {
		return balance;
	}

	public void setBalance(double balance) {
		this.balance = balance;
	}

	public PersonImpl getAccHolder() {
		return accHolder;
	}

	public void setAccHolder(PersonImpl accHolder) {
		this.accHolder = accHolder;
	}
	
	public Logger getLogger() {
		return logger;
	}

	public void setLogger(Logger logger) {
		this.logger = logger;
	}

	public void deposit(double amount) {
		if(logger != null)
			logger.log("Deposit " + amount +" in account " + accId);
		this.balance += amount;
	}
	
	public void withdraw(double amount) {
		if(logger != null)
			logger.log("Withdraw " + amount +" from account " + accId);
		this.balance -= amount;
	}

	@Override
	public String toString() {
		return String.format("AccountImpl [accId=%s, type=%s, balance=%s, accHolder=%s]", accId, type, balance,
				accHolder);
	}

}
